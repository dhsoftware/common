unit PlnUtil;

interface

uses  URecords, System.Generics.Collections, UInterfaces, UConstants, Util,
      System.Math, UDCLibrary;


PROCEDURE PlnReverse (var ent : entity);  overload;

PROCEDURE PlnFix (var ent : entity; isvoid : boolean);

FUNCTION OffsetPln (orig_pln : entity;
                    var pln  : entity;
                    existing : boolean;
                    OffsLeft : boolean;  // if sense is anti- clockwise then true will be to inside, false will be outside
                    offsdist : double) : boolean;   overload;

FUNCTION PointInPln (pnt : point; pln:entity) : integer;
FUNCTION LineInPln (Linpt1, Linpt2 : point; pln:entity) : boolean;

PROCEDURE AddFillet (var ent : entity; maxDis, maxPercent : double);

FUNCTION PlnGrossArea (frst, last : pvtaddr) : double;
PROCEDURE PlnPerim (frst, last : pvtaddr; OUT TotPerim, VoidPerim, NonVoidPerim : double);

FUNCTION DisFromPolyvert (pv : polyvert; tstpnt : point) : double;
FUNCTION DisFromPolyvert2 (pv : polyvert; tstpnt : point) : double;
FUNCTION DisFromPln (tstpnt : point; pln : entity) : double;

FUNCTION EntFitsInPline (Ent, Pline : entity) : boolean;
FUNCTION RotatedEntFitsInPline (Ent, Pline : entity; cntr : point; ang : double) : boolean;

FUNCTION FindPvSideFromPln (plnent : entity; testPnt : point;
                            out pvPoint : point; out pvt : polyvert) : boolean;
FUNCTION FindPvPntFromPln (plnent : entity; testPnt : point;
                           out pvPoint : point; var pvt : polyvert) : boolean;  overload;
FUNCTION FindPvPntFromPln (plnent : entity; testPnt : point; TightMissDiss : boolean;
                           out pvPoint : point; var pvt : polyvert) : boolean;  overload;

FUNCTION NextPv (plnent : entity; pv : polyvert) : polyvert;
FUNCTION PrevPv (plnent : entity; pv : polyvert) : polyvert;

FUNCTION PlnNumVertices (ent : entity) : integer;

FUNCTION PlnToPntArray (pln : entity; bulgedivs : integer; equaldivs : boolean;
                        var Visibility : TArray<boolean>;
                        ProcessMatrix : boolean = true;
                        pvertsVisOnly : boolean = false;
                        doVisibility : boolean = true) : TArray<point>;        overload;
  FUNCTION PlnToPntArray (pln : entity; bulgedivs : integer; equaldivs : boolean;
                          ProcessMatrix : boolean = true;
                          pvertsVisOnly : boolean = false) : TArray<point>;    overload;


implementation

PROCEDURE PlnReverse (var ent : entity; EnsureFirstStraight : boolean);  overload;
/// This proc will change the sense (direction) of the polyline.
///
/// If EnsureFirstStraight is true then additional logic will ensure that the first
/// straight side continues to be the first straight side in the reversed entity.
/// This is necessary as the macro uses the first straight side of a space to define
/// the virtual x-axis for measuring purposes.
TYPE
	pvrec = record
		shape : integer;
		pnt		: point;
		bulge	: real;
	end;
VAR
  pvr : pvrec;
	polyverts : TList<pvrec>;
	ndx : integer;
	addr : pvtaddr;
	pv		: polyvert;
  straightindex, i : integer;
BEGIN
  if (ent.enttype <> entpln) or addr_equal (ent.plnfrst, ent.plnlast) then
    exit;

  polyverts := TList<pvRec>.Create;
  try
    // first read all of the polyverts into our polyverts list
    addr := ent.plnfrst;
    while polyvert_get (pv, addr, ent.plnfrst) do begin
      addr := pv.next;
      pvr.shape := pv.shape;
      pvr.pnt := pv.pnt;
      pvr.bulge := pv.bulge;
      polyverts.Add(pvr);
    end;

    // find first straight side
    straightindex := -1;
    if EnsureFirstStraight then begin
      i := 0;
      while (i < polyverts.count) and (straightindex = -1) do begin
        if polyverts[i].shape = pv_vert then
          straightindex := i
        else i := i+1;
      end;
    end;
    if straightindex = -1 then
      straightindex := polyverts.Count-2;

    // now update each polyvert to have the attributes from polyverts array in reverse order
    addr := ent.plnfrst;
    ndx := (straightindex + 1) mod (polyverts.Count);
    while polyvert_get (pv, addr, ent.plnfrst) do begin
      pv.pnt := polyverts[ndx].pnt;
      ndx := ndx-1;
      if ndx < 0 then
        ndx := ndx + polyverts.Count;
      pv.nextpnt := polyverts[ndx].pnt;
      pv.shape := polyverts[ndx].shape;
      pv.bulge := -polyverts[ndx].bulge;
      polyvert_update (pv);
      addr := pv.next;
    end;
  finally
    polyverts.Free;
  end;
END;

PROCEDURE PlnReverse (var ent : entity);  overload;
BEGIN
  PlnReverse (ent, false);
END;



PROCEDURE PlnFix (var ent : entity; isvoid : boolean);
/// This proc will change the sense (direction) of the polyline if required.
/// if isvoid is false, then begin we want pline_area to return a +ve value, so the sense will be changed if initially pline_area is -ve
/// if isvoid is true then begin we want pline_area to return a -ve value, so the sense will be changed if initially it is +ve
///
///  There is also additional logic to ensure that the first straight side continues to
///  be the first straight side in the fixed entity.  This is necessary as the macro uses
///  the first straight side to define the virtual x-axis for measuring purposes.
BEGIN
  if (ent.enttype <> entpln) or addr_equal (ent.plnfrst, ent.plnlast) then
    exit;
	if (pline_area (ent.plnfrst, ent.plnlast)< 0.0) then begin
		if isvoid then
      exit;       // nothing to do
	end
  else begin
		if not isvoid then
      exit;       // nothing to do
	end;

  PlnReverse(ent, true);

END; //PlnFix;


FUNCTION OffsetPln (orig_pln : entity;
                    var pln  : entity;
                    existing : boolean;  // if true then existing offset pln will be updated, otherwise new entity will be created
                    OffsLeft : boolean;  // if sense is anti-clockwise then true will be to inside, false will be outside
                    offsdist : double) : boolean;        overload;

TYPE
  LineTyp = record
    shape : integer;
    pt1, pt2 : point;
    cntr : point;
    rad  : double;
    bang : double;
    eang : double;
    ccw  : boolean;
    visible : boolean;
  end;

VAR
  Line, PrevLine, NewLine : LineTyp;
  Lines, NewLines : TList<LineTyp>;
  pv : polyvert;
  adr : lgl_addr;
  direction : double;
  mov : point;
  inc : double;
  i, j   : integer;
  Line1, Line2 : LineTyp;
  numintr : integer;
  int1, int2, Lin1Pnt2 : point;
  bang1, eang1, bang2, eang2 : double;
  tempent : entity;
  ProcessVoids : boolean;
  VoidAnchor : point;
  StartingVoid : boolean;
  svInfo : byte;
BEGIN
  result := false;
  Lines := TList<LineTyp>.Create;

	// first read all of the sides into our lines array
	adr := orig_pln.plnfrst;
	while polyvert_get (pv, adr, orig_pln.plnfrst) do begin
		adr := pv.next;
    if (not pointsequal (pv.pnt, pv.nextpnt)) then begin
      Line.shape := pv.shape;
      Line.pt1 := pv.pnt;
      Line.pt2 := pv.nextpnt;
      if (Line.shape = pv_bulge) then begin
        bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, Line.cntr, Line.rad,
                      Line.bang, Line.eang, Line.ccw);
      end;
      Line.visible := (pv.info <> 1);
      Lines.add(Line);
		end;
	end;

  NewLines := TList<LineTyp>.Create;
  // create newlines offset to the the original lines in Lines list
  for i := 0 to Lines.Count-1 do begin
    Line := Lines[i];
    NewLine.shape := Line.shape;
    NewLine.visible := Line.visible;
    if Line.shape = pv_vert then begin
      direction := angle (Line.pt1, Line.pt2);
      if OffsLeft then
        direction := direction + halfpi
      else
        direction := direction - halfpi;

      cylind_cart (offsdist, direction, 0.0, mov.x, mov.y, mov.z);

      addpnt (Line.pt1, mov, NewLine.pt1);
      addpnt (Line.pt2, mov, NewLine.pt2);

      NewLines.Add(NewLine);
    end
    else begin
      if OffsLeft then
        inc := offsdist
      else
        inc := -offsdist;
      if Line.ccw then
        inc := -inc;

      NewLine.rad := Line.rad + inc;
      if NewLine.rad > 0 then begin
        NewLine.cntr := Line.cntr;
        NewLine.bang := Line.bang;
        NewLine.eang := Line.eang;
        NewLine.ccw := Line.ccw;
        NewLines.Add(NewLine);
      end
      else begin
        // substitute a straight segment for -ve radius curve
        NewLine.shape := pv_vert;
        cylind_cart (Line.rad, Line.bang, 0, mov.x, mov.y, mov.z);
        AddPnt (Line.cntr, mov, NewLine.pt1);
        cylind_cart (Line.rad, Line.eang, 0, mov.x, mov.y, mov.z);
        AddPnt (Line.cntr, mov, NewLine.pt2);

        direction := angle (NewLine.pt1, NewLine.pt2);
        if OffsLeft then
          direction := direction + halfpi
        else
          direction := direction - halfpi;

        cylind_cart (offsdist, direction, 0.0, mov.x, mov.y, mov.z);
        addpnt (NewLine.pt1, mov, NewLine.pt1);
        addpnt (NewLine.pt2, mov, NewLine.pt2);

        NewLines.Add(NewLine);
      end;
    end;
  end;

  Lines.Free;

  // adjust newlines to intersect with each other
  i := 0;
  while i < NewLines.Count - ord (not orig_pln.plnclose) do begin
    Line1 := NewLines[i];
    j := (i+1) mod NewLines.Count;
    Line2 := NewLines[j];

    if (Line1.shape = pv_vert) and (Line2.shape = pv_vert) then begin
      if dis_from_line (Line1.pt1, Line1.pt2, Line2.pt1) < 0.0001 then begin
        // line points are effectively colinear or else lines already join
        Line1.pt2 := Line2.pt1;
        NewLines[i] := Line1;
      end
      else begin
        if not intr_linlin (Line1.pt1, Line1.pt2, Line2.pt1, Line2.pt2, int1, false) then begin
          NewLines.Free;
          exit;
        end
        else begin
          if orig_pln.plnclose or (j < (NewLines.Count-1)) then begin
            Line1.pt2 := int1;
            NewLines[i] := Line1;
            Line2.pt1 := int1;
            NewLines[j] := Line2;
          end
          else begin
            Line2.pt1 := Line1.pt2;     // logic that creates the new polyline uses this point rather than 2nd point of line1
            NewLines[j] := Line2;
          end;
        end;

      end;
    end
    else if (Line1.shape = pv_vert) and (Line2.shape = pv_bulge) then begin
      numintr := intr_lincrc (Line2.cntr, Line2.rad, Line1.pt1, Line1.pt2,
                              int1, int2, false);

      if numintr = 2 then begin  // use closest point to Line1.pt2 (????)
        if distance (Line1.pt2, int1) > distance (Line1.pt2, int2) then
          int1 := int2;
      end;
      if numintr > 0 then begin
        Line1.pt2 := int1;
        NewLines[i] := Line1;
        Line2.bang := angle (Line2.cntr, int1);
        NewLines[j] := Line2;
      end
      else begin // no intersection, insert a straight segment
        NewLine.shape := pv_vert;
        NewLine.pt1 := Line1.pt2;
        cylind_cart (Line2.rad, Line2.bang, 0.0, mov.x, mov.y, mov.z);
        AddPnt (Line2.cntr, mov, NewLine.pt2);
        NewLine.visible := Line1.visible or Line2.visible;
        if j=0 then
          NewLines.Add(NewLine)
        else
          NewLines.Insert(j, NewLine);
        i := i+1; // skip to next line for next iteration
      end;
    end

    else if (Line1.shape = pv_bulge) and (Line2.shape = pv_vert) then begin
      numintr := intr_lincrc (Line1.cntr, Line1.rad, Line2.pt1, Line2.pt2,
                              int1, int2, false);

      if numintr = 2 then begin  // use closest point to Line2.pt1 (????)
        if distance (Line2.pt1, int1) > distance (Line2.pt1, int2) then
          int1 := int2;
      end;
      if numintr > 0 then begin
        Line2.pt1 := int1;
        NewLines[j] := Line2;
        Line1.eang := angle (Line1.cntr, int1);
        NewLines[i] := Line1;
      end
      else begin // no intersection, insert a straight segment
        NewLine.shape := pv_vert;
        NewLine.pt2 := Line2.pt1;
        cylind_cart (Line1.rad, Line1.eang, 0.0, mov.x, mov.y, mov.z);
        AddPnt(Line1.cntr, mov, NewLine.pt1);
        NewLine.visible := Line1.visible or Line2.visible;
        if j=0 then
          NewLines.Add(NewLine)
        else
          NewLines.Insert(j, NewLine);
        i := i+1; // skip this new line for next iteration
      end;
    end
    else begin   // both lines are curves
      numintr := intr_crccrc (Line1.cntr, Line2.cntr, Line1.rad, Line2.rad, int1, int2);

      if numintr = 2 then begin // use point closes to the end of line 1
        cylind_cart (line1.rad, line1.eang, 0, mov.x, mov.y, mov.z);
        AddPnt (line1.cntr, mov, Lin1Pnt2);
        if distance (Lin1Pnt2, int2) < distance (Lin1Pnt2, int1) then
          int1 := int2;
      end;
      if numintr > 0 then begin
        Line1.eang := angle (Line1.cntr, int1);
        NewLines[i] := Line1;
        Line2.bang := angle (Line2.cntr, int1);
        NewLines[j] := Line2;
      end
      else begin
        // insert a straight segment between the arcs
        NewLine.shape := pv_vert;
        cylind_cart (line1.rad, line1.eang, 0, mov.x, mov.y, mov.z);
        AddPnt (line1.cntr, mov, NewLine.pt1);
        cylind_cart (line2.rad, line2.bang, 0, mov.x, mov.y, mov.z);
        AddPnt (line2.cntr, mov, NewLine.pt2);
        NewLine.visible := Line1.visible or Line2.visible;
        if j=0 then
          NewLines.Add(NewLine)
        else
          NewLines.Insert(j, NewLine);
        i := i+1; // skip this new line for next iteration
      end;
    end;
    i := i+1;
  end;

  if NewLines.Count = 0 then begin
    // don't think this can ever happen, but just in case ...
    NewLines.Free;
    exit;
  end;

  if existing then begin
    i := 0;
    svInfo := 0;
    adr := pln.plnfrst;
    while polyvert_get (pv, adr, pln.plnfrst) do begin
      adr := pv.Next;
      if i = 0 then
        svInfo := pv.info;
      if i < NewLines.Count then begin
        NewLine := NewLines[i];
        pv.shape := NewLine.shape;
        if pv.shape = pv_vert then begin
          pv.bulge := 0.0;
          pv.pnt := NewLine.pt1;
          pv.nextpnt := NewLine.pt2;
        end
        else
          arc_to_bulge (NewLine.cntr, NewLine.rad, NewLine.bang, NewLine.eang, NewLine.ccw,
                        pv.pnt, pv.nextpnt, pv.bulge);
        polyvert_update (pv);
      end
      else
        polyvert_del (pv, pln.plnfrst, pln.plnlast);

      i := i+1;
    end;

    while (i < NewLines.Count) do begin
      NewLine := NewLines[i];
      polyvert_init (pv);
      pv.info := svInfo;
      pv.shape := NewLine.shape;
      if pv.shape = pv_vert then begin
        pv.bulge := 0.0;
        pv.pnt := NewLine.pt1;
        pv.nextpnt := NewLine.pt2;
      end
      else
        arc_to_bulge (NewLine.cntr, NewLine.rad, NewLine.bang, NewLine.eang, NewLine.ccw,
                      pv.pnt, pv.nextpnt, pv.bulge);
      polyvert_add (pv, pln.plnfrst, pln.plnlast);
      i := i+1;
    end;

  end
  else begin
    // create a new polyline based on the NewLines list
    ent_init (pln, entpln);
    for i := 0 to NewLines.Count-1 do begin
      NewLine := NewLines[i];
      polyvert_init (pv);
      pv.shape := NewLine.shape;
      if not NewLine.visible then
        pv.info := 1;
      if pv.shape = pv_vert then begin
        pv.bulge := 0.0;
        pv.pnt := NewLine.pt1;
        pv.nextpnt := NewLine.pt2;
      end
      else begin
        arc_to_bulge (NewLine.cntr, NewLine.rad, NewLine.bang, NewLine.eang, NewLine.ccw,
                      pv.pnt, pv.nextpnt, pv.bulge);
      end;
      polyvert_add (pv, pln.plnfrst, pln.plnlast);
    end;
  end;

  pln.plbase := orig_pln.plbase;
  pln.plnhite := orig_pln.plnhite;

  if existing then
    ent_update (pln)
  else begin
    pln.plnclose := orig_pln.plnclose;
    ent_add (pln);
  end;

  NewLines.Free;
  result := true;
END;


FUNCTION PointInPln (pnt : point; pln:entity) : integer;
/// returns -1 if point is outside
/// returns 0 is point is on polyline
/// returns 1 is point is inside
/// returns -2 in pln entity is not a valid polyline entity
TYPE
  LinCurv = Record
    shape : integer;  //pv_vert or pv_bulge
    p1, p2: point;
    cntr  : point;
    radius: double;
    bang,
    eang  : double;
  end;
VAR
	i					: integer;
	ospnt					: point;	// a point set up to be outside ply
  minpt, maxpt  : point;
  pvaddr        : lgl_addr;
  pv            : polyvert;
  ccw           : boolean;
  Line          : LinCurv;
  LineList      : tList <LinCurv>;
  numIntersec   : integer;

  satisfactory	: boolean;
	int1, int2		: point;
	xcount				: integer;
	trycount			: integer;

BEGIN
  if pln.enttype <> entpln then begin
    result := -2;
    exit;
  end;

  ent_extent (pln, minpt, maxpt);
  if (pnt.x < minpt.x) or (pnt.x > maxpt.x) or
     (pnt.y < minpt.y) or (pnt.y > maxpt.y) then begin
    result := -1;
    exit;
  end;

  // for convenience convert the pln to a list of lines/curves
  LineList := tList<LinCurv>.Create;
  try
    pvaddr := pln.plnfrst;
    while polyvert_get (pv, pvaddr, pln.plnfrst) do begin
      pvaddr := pv.Next;
      Line.shape := pv.shape;
      Line.p1 := pv.pnt;
      Line.p2 := pv.nextpnt;
      if pv.shape = pv_bulge then begin
        bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, Line.cntr, Line.radius,
                      line.bang, line.eang,ccw);
        if not ccw then
          swapaFloat (Line.bang, line.eang);
      end;
      LineList.Add(Line);
    end;

    // set up ospnt to a point outside the bounding rectangle
    ospnt.x := minpt.x - 1.0;
    ospnt.y := (maxpt.y + minpt.y) / 2;
    ospnt.z := pln.plbase;

    // check how many times a line from ospnt to pnt crosses ply boundary
    // An odd number of times indicates that pnt is inside ply
    // An even number of times indicates that pnt is outside ply
    trycount := 0;
    repeat
      satisfactory := true;
      trycount := trycount+1;
      xcount := 0;
      for i := 0 to LineList.Count-1 do begin
        if LineList[i].shape = pv_bulge then begin
          if dis_from_arc (LineList[i].cntr, LineList[i].radius,
                           LineList[i].bang, LineList[i].eang, pnt) < abszero then begin
            result := 0;
            exit;
          end;
          numIntersec := intr_linarc (LineList[i].cntr, LineList[i].radius, LineList[i].bang,
                                      LineList[i].eang, pnt, ospnt, int1, int2, true);
          if (numIntersec = 1) and
             realequal (distance (pnt, LineList[i].cntr), LineList[i].radius) then begin
            // it is extremely likely that the line is tangential to the arc
            satisfactory := false;
            numIntersec := 0;
          end;
          if (numIntersec = 1) and PointsEqual(int1, LineList[i].p1) then begin
            satisfactory := false;
            numIntersec := 0; // should already be counted on p2 of the previous line
          end;
          xcount := xcount + numIntersec;
        end
        else begin
          if intr_linlin (ospnt, pnt, LineList[i].p1, LineList[i].p2, int1, true) then begin
            if PointsEqual (int1, LineList[i].p1) then begin
              satisfactory := false;
              numIntersec := 0; // should already be counted on p2 of the previous line
            end
            else
              numIntersec := 1;
          end
          else
            numIntersec := 0;

          xcount := xcount + numIntersec;
        end;
      end;
      if not satisfactory then begin
        // change ospnt to a new position
        ospnt.y := ospnt.y + trycount;
        if trycount > 5 then
          ospnt.x := maxpt.x + 1;
        if trycount = 11 then
          ospnt.y := maxpt.y;
      end;
    until satisfactory or (trycount > 12);    // limit to 12 to avoid posibility of endless loop

    if xcount mod 2 = 0 then
      result := -1
    else
      result := 1;

  finally
    LineList.Free;
  end;

END;





FUNCTION LineInPln (Linpt1, Linpt2 : point; pln:entity) : boolean;
TYPE
  LinCurv = Record
    shape : integer;  //pv_vert or pv_bulge
    p1, p2: point;
    cntr  : point;
    radius: double;
    bang,
    eang  : double;
  end;
VAR
	i					: integer;
  minpt, maxpt  : point;
  pvaddr        : lgl_addr;
  pv            : polyvert;
  ccw           : boolean;
  Line          : LinCurv;
  LineList      : tList <LinCurv>;

	int1, int2		: point;

BEGIN
  if pln.enttype <> entpln then begin
    result := false;
    exit;
  end;

  ent_extent (pln, minpt, maxpt);
  if (Linpt1.x < minpt.x) or (Linpt1.x > maxpt.x) or
     (Linpt1.y < minpt.y) or (Linpt1.y > maxpt.y) or
     (Linpt2.x < minpt.x) or (Linpt2.x > maxpt.x) or
     (Linpt2.y < minpt.y) or (Linpt2.y > maxpt.y) then begin
    result := false;
    exit;
  end;

  if PointInPln(Linpt1, pln) < 1 then begin
    result := false;
    exit;
  end;
  if PointInPln(Linpt2, pln) < 1 then begin
    result := false;
    exit;
  end;

  // for convenience convert the pln to a list of lines/curves
  LineList := tList<LinCurv>.Create;
  try
    pvaddr := pln.plnfrst;
    while polyvert_get (pv, pvaddr, pln.plnfrst) do begin
      pvaddr := pv.Next;
      Line.shape := pv.shape;
      Line.p1 := pv.pnt;
      Line.p2 := pv.nextpnt;
      if pv.shape = pv_bulge then begin
        bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, Line.cntr, Line.radius,
                      line.bang, line.eang,ccw);
        if not ccw then
          swapaFloat (Line.bang, line.eang);
      end;
      LineList.Add(Line);
    end;

    for i := 0 to LineList.Count-1 do begin
      if LineList[i].shape = pv_bulge then begin
        if intr_linarc (LineList[i].cntr, LineList[i].radius, LineList[i].bang, LineList[i].eang,
                        Linpt1, Linpt2, int1, int2, true) > 0 then begin
          result := false;
          exit;
        end;
      end
      else begin
        if intr_linlin (Linpt1, LinPt2, LineList[i].p1, LineList[i].p2, int1, true) then begin
          result := false;
          exit;
        end;
      end;
    end;

    result := true;

  finally
    LineList.Free;
  end;

END;

PROCEDURE AddFillet (var ent : entity; maxDis, maxPercent : double);
/// This procedure will add a fillet curve between the last 2 straight
/// segments of a polyline.
///  It also stores the original length of the last side in the bulge field of
///  the polyvert (this field is not used by DataCAD for anything else in the
///  case of straight sides)
VAR
  pv, prevpv, prevprevpv, newpv : polyvert;
  NewPrevPrevp2, NewPrevp1, temppnt, cntr : point;
  tempent, tempent1 : entity;
  maxPercentDis : double;
  ccw : boolean;
BEGIN
  if maxPercent > 49.01 then
    maxPercent := 49.0;
  if maxPercent < 0.01 then
    maxPercent := 0.01;
  if maxDis < 0 then
    maxDis := 0;     // value of 0 indicates there is no maximum

  if ent.enttype <> entpln then
    exit;
  if addr_equal(ent.plnfrst, ent.plnlast) then
    exit; // only 1 polyvert, not enought for this process
  if not polyvert_get (pv, ent.plnlast, ent.plnfrst) then
    exit;
  if addr_equal (ent.plnfrst, pv.prev) then
    exit; // only 2 polyverts, not enough for this process
  if not polyvert_get (prevpv, pv.prev, ent.plnfrst) then
    exit;
  if not polyvert_get (prevprevpv, prevpv.prev, ent.plnfrst) then
    exit;
  if (pv.shape <> pv_vert) or (prevpv.shape <> pv_vert) or
     (prevprevpv.shape <> pv_vert) then
    exit; // can only insert fillet between straight sides

  pv.bulge := distance (prevpv.pnt, pv.pnt);
  polyvert_update (pv);
  if prevpv.bulge = 0 then
    prevpv.bulge := distance (prevpv.pnt, prevprevpv.pnt);
  if pv.bulge < prevpv.bulge then
    maxPercentDis := pv.bulge * maxPercent/100
  else
    maxPercentDis := prevpv.bulge * maxPercent/100;
  if (MaxDis = 0) or (MaxDis > maxPercentDis) then
    MaxDis := maxPercentDis;

  // find points on existing sides where fillet will start
  if intr_lincrc (prevpv.pnt, MaxDis,  prevprevpv.pnt, prevpv.pnt,
                  NewPrevPrevp2, temppnt, true) <> 1 then
    exit;
  if intr_lincrc (prevpv.pnt, MaxDis,  prevpv.pnt, pv.pnt,
                  NewPrevp1, temppnt, true) <> 1 then
    exit;

  // find center of fillet
  ent_init (tempent, entlin);
  tempent.linpt1 := NewPrevPrevp2;
  tempent.linpt2 := prevprevpv.pnt;
  ent_rotate (tempent, NewPrevPrevp2, halfpi);
  ent_init (tempent1, entlin);
  tempent1.linpt1 := NewPrevp1;
  tempent1.linpt2 := pv.pnt;
  ent_rotate (tempent1, NewPrevp1, halfpi);
  if not intr_linlin (tempent.linpt1, tempent.Linpt2, tempent1.linpt1, tempent1.linpt2,
                      cntr, false) then
    exit;

  // work out if fillet is counter-clockwise or clockwise
  ccw := crossz (prevprevpv.pnt, prevpv.pnt, cntr) > 0;

  // create fillet
  polyvert_init (newpv);
  newpv.shape := pv_bulge;
  newpv.pnt := NewPrevPrevp2;
  newpv.nextpnt := NewPrevp1;
  arc_to_bulge (cntr, distance(cntr, NewPrevp1),
                angle(cntr, NewPrevPrevp2), angle(cntr, NewPrevp1), ccw,
                NewPrevPrevp2, NewPrevp1, newpv.bulge);
  polyvert_ins (newpv, prevprevpv.addr, ent.plnfrst, ent.plnlast);
  prevpv.pnt := NewPrevp1;
  polyvert_update (prevpv);

END;





FUNCTION GetxAxisAngle (ent : entity) : double;
/// finds the xAxis angle based on the first straight side in the polyline
VAR
  pv : polyvert;
  addr : lgl_addr;
BEGIN
  result := 0;
  if ent.enttype <> entpln then
    exit;

  addr := ent.plnfrst;
  while polyvert_get (pv, addr, ent.plnfrst) do begin
    addr := pv.Next;
    if pv.shape = pv_vert then begin
      result := angle (pv.pnt, pv.nextpnt);
      while result < 0 do
        result := result + Pi;
      while result >= pi do
        result := result - pi;
      if RealEqual (result, pi, 0.0000001) or RealEqual (result, 0, 0.0000001) then
        result := 0;
      exit;
    end;
  end;
END;


FUNCTION PlnGrossArea (frst, last : pvtaddr) : double;
VAR
  addr : pvtaddr;
	pv, tempPV : polyvert;
	firstpnt : point;
  first : boolean;
  Tempent : entity;
BEGIN
  ent_init (tempent, entpln);
	first := true;
	addr := frst;
	while polyvert_get (pv, addr, frst) do begin
		addr := pv.next;
    if first then begin
      firstpnt := pv.pnt;
      first := false;
    end
    else begin
      if PointsEqual(pv.pnt, firstpnt) and not addr_equal (pv.prev, frst) then begin
        result := pline_area (tempent.plnfrst, tempent.plnlast);
        exit;
      end;
    end;
    polyvert_init (tempPV);
    tempPV.pnt := pv.pnt;
    tempPV.shape := pv.shape;
    tempPV.bulge := pv.bulge;
    polyvert_add (tempPV, tempent.plnfrst, tempent.plnlast);
  end;
  result := pline_area (frst, last);
END;


PROCEDURE PlnPerim (frst, last : pvtaddr; OUT TotPerim, VoidPerim, NonVoidPerim : double);
/// Due to the way I create 'voids', I cannot just use the DCAL supplied pline_perim function to get the perimeter. I need
/// to subtract the 'invisible' lines associated with the voids.
VAR
	perim,
	invisPerim,
	vdPerim : double;
	first : boolean;
	addr : pvtaddr;
	pv : polyvert;
	firstpnt : point;
	voidpnt1 : point;
	voidpvt1 : pvtaddr;
	invoid : boolean;
BEGIN
	perim := pline_perim (frst, last);
	vdPerim := 0.0;
	invisPerim := 0.0;
	first := true;
	addr := frst;
  invoid := false;

	WHILE polyvert_get (pv, addr, frst) DO BEGIN
		addr := pv.next;
		if first then begin
			firstpnt := pv.pnt;
			first := false;
    end
		else if PointsEqual (firstpnt, pv.pnt) then begin
			invoid := not pv.last;
			if invoid then begin
				voidpnt1 := pv.nextpnt;
				voidpvt1 := pv.next;
			end;
    end
		else if invoid and PointsEqual (firstpnt, pv.nextpnt) and
            PointsEqual (voidpnt1, pv.pnt) then begin
			vdPerim := vdPerim + pline_perim (voidpvt1, pv.addr);
			invoid := false;
			invisperim := invisperim + 2.0 * distance (voidpnt1, firstpnt);
		end;
	END;
	TotPerim := perim - invisperim;
	VoidPerim := vdPerim;
	NonVoidPerim := perim - invisperim - vdPerim;
END; // PlnPerim

FUNCTION DisFromPolyvert2 (pv : polyvert; tstpnt : point) : double;
/// returns the square of the distance
VAR
  cent : point;
  radius, bang, eang, r : double;
  ccw : boolean;
BEGIN
  if pv.shape = pv_vert then begin
    result := dis_from_seg (pv.pnt, pv.nextpnt, tstpnt);
  end
  else begin
    bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, cent, radius, bang, eang, ccw);
    if not ccw then begin
      r := bang;
      bang := eang;
      eang := r;
    end;
    result := dis_from_arc (cent, radius, bang, eang, tstpnt);
  end;
END;

FUNCTION DisFromPolyvert (pv : polyvert; tstpnt : point) : double;
BEGIN
  result := sqrt (DisFromPolyvert2(pv, tstpnt));
END;


FUNCTION DisFromPln (tstpnt : point; pln : entity) : double;
VAR
  pv : polyvert;
  adr : lgl_addr;
BEGIN
  adr := pln.plnfrst;
  result := MaxDouble;
  while polyvert_get (pv, adr, pln.plnfrst) do begin
    adr := pv.Next;
    result := Min (result, DisFromPolyvert (pv, tstpnt));
  end;
END;


FUNCTION FitsInPline (Pline : entity; Pnts : pntarr; NumPnts : integer) : boolean;
VAR
  i, j : integer;
BEGIN
  for i := low(Pnts) to  low(Pnts)+NumPnts-1 do begin
    j := i+1;
    if j > low(Pnts)+NumPnts-1 then
      j := low(Pnts);
    if not LineInPln (Pnts[i], Pnts[j], pline) then begin
      result := false;
      exit;
    end;
  end;

  result := true;
END;

FUNCTION EntFitsInPline (Ent, Pline : entity) : boolean;
VAR
  entbox : pntarr;
  i : integer;
BEGIN
  Case Ent.enttype of
      enttxt :
        begin
          txtbox (ent,  entbox[1], entbox[3], entbox[4], entbox[2]);
          result := FitsInPline (pline, entbox, 4);
        end;
      entlin :
        begin
          entbox[1] := ent.linpt1;
          entbox[2] := ent.linpt2;
          result := FitsInPline (pline, entbox, 2);
        end;
      entln3 :
        begin
          entbox[1] := ent.ln3pt1;
          entbox[2] := ent.ln3pt2;
          result := FitsInPline (pline, entbox, 2);
        end;
      entply :
        result := FitsInPline (pline, ent.plypnt, ent.plynpnt);
      entslb :
        begin
          result := FitsInPline (pline, ent.slbpnt, ent.slbnpnt);
          if result then begin
            for i := 1 to ent.slbnpnt do
              AddPnt (ent.slbpnt[i], ent.slbthick, entbox[i]);
            result := FitsInPline (pline, entbox, ent.slbnpnt);
          end;
        end
      else
        begin
          ent_extent (ent, entbox[1], entbox[3]);
          entbox[2].x := entbox[1].x;
          entbox[2].y := entbox[3].y;
          entbox[2].z := entbox[1].z;
          entbox[4].x := entbox[3].x;
          entbox[4].y := entbox[1].y;
          entbox[4].z := entbox[1].z;
          result := FitsInPline (pline, entbox, 4);
        end;
  End;
END;

FUNCTION RotatedEntFitsInPline (Ent, Pline : entity; cntr : point; ang : double) : boolean;
VAR
  svlyr, templyr : lgl_addr;
  tempent : entity;
BEGIN
  if ent.enttype in [entpln, entsrf] then begin
    //need to take a temp copy of these entity types as ent_rotate actual updates the polyverts
    svlyr := getlyrcurr;
    lyr_init (templyr);
    lyr_set (templyr);
    try
      ent_copy (ent, tempent, true, false);
      ent_rotate (Tempent, cntr, ang);
      result := EntFitsInPline (ent, Pline);
      lyr_clear (templyr);
      lyr_set (svlyr);
    finally
      lyr_term (templyr);
    end;
  end
  else begin
    ent_rotate (Ent, cntr, ang);
    result := EntFitsInPline (ent, Pline);
  end;
END;

FUNCTION FindPvSideFromPln (plnent : entity; testPnt : point;
                            out pvPoint : point; out pvt : polyvert) : boolean;
VAR
  adr : lgl_addr;
  pv : polyvert;
  missdis2, currentdis2, newdis2 : double;
BEGIN
  missdis2 := sqr(pgSaveVar^.missdis * pixsize);
  currentdis2 := missdis2+1;
  adr := plnent.plnfrst;
  while polyvert_get (pv, adr, plnent.plnfrst) do begin
    adr := pv.Next;
    newdis2 := DisFromPolyvert2 (pv, testPnt);
    if newdis2 < currentdis2 then begin
      result := true;
      pvt := pv;
      pvPoint := pv.pnt;
      exit;
    end;
  end;
  result := false;
END;

FUNCTION FindPvPntFromPln (plnent : entity; testPnt : point; TightMissDiss : boolean;
                           out pvPoint : point; var pvt : polyvert) : boolean;  overload;
/// Finds a polyvert on the passed plnent which is within the standard miss distance of testPnt
/// If TightMissDiss is true then it also tests that testPnt is within 1/3 of the side length
/// of the polyvert point (this is necessary to allow the user to pick a short side in some
/// logic which allows for selection of either a side OR a vertex ... without this flag being
/// set it was not possible to select a side which was displayed less than 2 x missdis pixels
/// long)
VAR
  adr : lgl_addr;
  pv, pvprev : polyvert;
  missdis, currentdis, newdis : double;
BEGIN
  missdis := pgSaveVar^.missdis * pixsize;
  currentdis := missdis+1;
  adr := plnent.plnfrst;
  while polyvert_get (pv, adr, plnent.plnfrst) do begin
    newdis := distance (testPnt, pv.pnt);
    adr := pv.Next;
    if newdis < currentdis then begin
      currentdis := newdis;
      pvt := pv;
      pvPoint := pv.pnt;
    end;
  end;
  result := (currentdis <= missdis);
  if result and TightMissDiss then begin
    // reduce allowed missdis so user has a chance to select the side in subsequent logic
    pvPrev := PrevPv(plnent, pvt);
    if DisFromPolyvert (pvt, testpnt) < DisFromPolyvert (pvPrev, testpnt) then
      result := (currentdis <= distance(pvt.pnt, pvt.nextpnt)/3)
    else
      result := (currentdis <= distance (pvt.pnt, pvPrev.pnt)/3);
  end;
END;


FUNCTION FindPvPntFromPln (plnent : entity; testPnt : point;
                           out pvPoint : point; var pvt : polyvert) : boolean;
BEGIN
  result := FindPvPntFromPln (plnent, testPnt, false, pvPoint, pvt);
END;


  FUNCTION PrevPv (plnent : entity; pv : polyvert) : polyvert;
  /// finds the 'Previous' polyvert, taking into account the way I have implemented voids
  VAR
    adr : lgl_addr;
  BEGIN
    if isnil (pv.prev) then begin
      adr := pv.Next;
      while polyvert_get (result, adr, plnent.plnfrst) do begin
        adr := result.Next;
        if PointsEqual (pv.pnt, result.nextpnt) then
          exit;
      end;
    end
    else begin
      polyvert_get (result, pv.prev, plnent.plnfrst);
    end;
  END;

  FUNCTION NextPv (plnent : entity; pv : polyvert) : polyvert;
  BEGIN
    if not polyvert_get (result, pv.Next, plnent.plnfrst) then
      polyvert_get (result, plnent.plnfrst, plnent.plnfrst);
  END;



  FUNCTION PlnNumVertices (ent : entity) : integer;
  VAR
    pv : polyvert;
  BEGIN
    result := 0;
    if polyvert_get (pv, ent.plnfrst, ent.plnfrst) then begin
      result := 1;
      while polyvert_get (pv, pv.Next, ent.plnfrst) do
        result := result + 1;
    end;
  END;


FUNCTION PlnToPntArray (pln : entity; bulgedivs : integer; equaldivs : boolean;
                        var Visibility : TArray<boolean>;
                        ProcessMatrix : boolean = true;
                        pvertsVisOnly : boolean = false;
                        doVisibility : boolean = true) : TArray<point>;
  var
    len : integer;
    pv : polyvert;
    pvadr : pvtaddr;
    cntr, pnt : point;
    rad, bang, eang, ang, anginc : double;
    ccw : boolean;

    procedure AddPnt (pnt : point; visible : boolean = true);
    begin
      if len >= length(result) then
        setlength (result, len + 128);
      result[len] := pnt;
      if doVisibility then begin
        if len >= length(Visibility) then
          SetLength(Visibility, len + 128);
        Visibility[len] := visible;
      end;
      len := len+1;
    end;

  begin
    setlength (result, 0);
    len := 0;
    if not ent_get (pln, pln.addr) then
      exit;
    if pln.enttype <> entpln then
      exit;

    pvadr := pln.plnfrst;
    while polyvert_get (pv, pvadr, pln.plnfrst) do begin
      if ProcessMatrix then
        xformpt (pv.pnt, pln.Matrix, pnt)
      else
        pnt := pv.pnt;
      AddPnt (pnt, (pv.info <> 1) and ((pv.shape <> pv_bulge) or (not pvertsVisOnly)));     // info value of 1 = invisible
      if (pv.shape = pv_bulge) and ((not pv.last) or pln.plnclose) then
        if (*not pvertsOnly*) true then begin
          bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, cntr, rad, bang, eang, ccw);
          while ccw and (bang > eang) do
            eang := eang + TwoPi;
          while (not ccw) and (eang > bang) do
            bang := bang + TwoPi;

          anginc := (eang - bang) / ceil(abs((eang - bang)/TwoPi*bulgedivs));
          ang := bang + anginc;
          while not RealEqual (ang, eang, abs(anginc/2)) do begin
            cylind_cart (rad, ang, pv.pnt.z, pnt.x, pnt.y, pnt.z);
            if ProcessMatrix then
              xformpt (AddPoints (cntr, pnt), pln.Matrix, pnt)
            else
              pnt := AddPoints (cntr, pnt);
            AddPnt (pnt, (pv.info<>1) and not pvertsVisOnly);
            ang := ang + anginc;
          end;
        end;
      pvadr := pv.Next;
    end;
    setlength (result, len);
  end;

  FUNCTION PlnToPntArray (pln : entity; bulgedivs : integer; equaldivs : boolean;
                          ProcessMatrix : boolean = true; pvertsVisOnly : boolean = false) : TArray<point>;
  var
   dummy : TArray<boolean>;
  begin
    result := PlnToPntArray (pln, bulgedivs, equaldivs, dummy, ProcessMatrix, pvertsVisOnly, false);
  end;


end.

