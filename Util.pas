unit Util;

interface

uses URecords, UInterfaces, UConstants, UInterfacesRecords, SysUtils,
     Winapi.Windows, Vcl.Graphics, System.Math;


FUNCTION RealEqual (const r1, r2, delta : double) : boolean;  overload;
FUNCTION RealEqual (const r1, r2 : double) : boolean;  overload;
function RealEqual3 (const r1, r2, r3, delta : double) : boolean;  overload;
function RealEqual3 (const r1, r2, r3 : double) : boolean;  overload;

FUNCTION addr_equal (addr1, addr2 : lgl_addr) : boolean;

FUNCTION PointsEqual (p1, p2 : URecords.point; delta : double; TestZ : boolean) : boolean;     overload;
FUNCTION PointsEqual (pfirstadr, p1adr, p2adr : lgl_addr; delta : double; TestZ : boolean) : boolean; overload;
FUNCTION PointsEqual (p1, p2 : URecords.point; TestZ : boolean) : boolean;      overload;
FUNCTION PointsEqual (p1, p2 : URecords.point; delta : double) : boolean;    overload;
FUNCTION PointsEqual (p1, p2 : URecords.point) : boolean;     overload;
FUNCTION PointsEqual (pnt1, pnt2, pnt3 : point; delta : double; TestZ : boolean) : boolean;   overload;
FUNCTION PointsEqual (pnt1, pnt2, pnt3 : point; TestZ : boolean) : boolean;   overload;
FUNCTION PointsEqual (p1, p2 : URecords.point2D; delta : double) : boolean;  overload;
FUNCTION PointsEqual (p1, p2 : URecords.point2D) : boolean;  overload;

function AddPoints (const pnt1, pnt2 : point) : point;
function MulPoint (const pnt1 : point; multiplier : double) : point;

function NewPoint (const xval, yval, zval : double) : point; overload;
function NewPoint (const xyval : point; zval : double) : point; overload;
function NewPoint (const xyval : Point2D; zval : double) : point; overload;
function NewPoint (const xyval : Point2D) : point; overload;
function SubPnt2D (const p1, p2 : point) : Point2D;  overload;
function SubPnt2D (const p1, p2 : Point2D) : Point2D;  overload;
function SubPnt2D (const p1 : Point2D; const p2 : point) : Point2D;  overload;
function NewPnt2D (const pnt : point) : Point2D;

function isUnityMatrix (const mat : modmat) : boolean;

function DisStr (dis : double) : string;

/// <summary> Calculate the square of the distance between the 2 points.  I expect this will be faster than using the
///           Distance function where the square of the distance is adequate
/// </summary>
function Distance2 (const p1, p2 : point) : double;

/// <summary> calculate a transformation that can be applied to move the passed pntarr to a horizontal plane
/// </summary>
/// <param name="pnts">The pntarr to process.  If should represent planar points (if not then the returned matrix may move as few as 3 points to the horizontal plane).
/// <returns>A modelling matrix that will transform the pntarr to a horizontal plane.
/// </returns>
function GetHorizontalMat (const pnts : pntarr; npnts : integer) : modmat;


FUNCTION ColourName (clr : longint) : shortstring;

PROCEDURE DrawIfOn (ent : entity; drMode : integer);

/// <summary> Draw the passed entity in the colour and line type specified with a slightly increased weight
/// </summary>
/// <param name="ent">The entity to draw.
/// <param name="clr">The colour to draw the entity with. Zero (0) indicates to unhilite the entity (draw it with original linetype/colour/weight)
/// <param name="ltype">The line type to draw the entity with. Not applicagle with clr of 0.
procedure Hilite (ent : entity; clr : integer; ltype : integer = ltype_dashed);

procedure MyModeInit (var mode : mode_type);

procedure ValidFileName (var filename : string);

procedure ReversePnts (var pnts : TArray<point>);



implementation

FUNCTION RealEqual (const r1, r2, delta : double) : boolean;
BEGIN
  result := abs(r1-r2) < delta;
END;

FUNCTION RealEqual (const r1, r2 : double) : boolean;
BEGIN
  result := abs(r1-r2) < 0.00001;
END;

function RealEqual3 (const r1, r2, r3, delta : double) : boolean;
begin
  result := (abs (r1-r2) <  delta) and (abs (r1-r3) <  delta) and (abs (r2-r3) <  delta);
end;

function RealEqual3 (const r1, r2, r3 : double) : boolean;
begin
  result := RealEqual3 (r1, r2, r3, 0.00001);
end;


FUNCTION addr_equal (addr1, addr2 : lgl_addr) : boolean;
BEGIN
  result := (addr1.page = addr2.page) and (addr1.ofs = addr2.ofs);
END;

FUNCTION PointsEqual (p1, p2 : URecords.point; delta : double; TestZ : boolean) : boolean;
BEGIN
  result := RealEqual(p1.x , p2.x, delta) and RealEqual(p1.y, p2.y, delta);
  if result and TestZ then
	result := RealEqual (p1.z, p2.z, delta);
END;

FUNCTION PointsEqual (pfirstadr, p1adr, p2adr : lgl_addr; delta : double; TestZ : boolean) : boolean;
VAR
  pv1, pv2 : polyvert;
  p1, p2 : URecords.point;
BEGIN
  polyvert_get (pv1, p1adr, pfirstadr);
  polyvert_get (pv2, p2adr, pfirstadr);
  result := RealEqual(pv1.pnt.x , pv2.pnt.x, delta) and RealEqual(pv1.pnt.y, pv2.pnt.y, delta);
  if result and TestZ then
	result := RealEqual (p1.z, p2.z, delta);
END;


FUNCTION PointsEqual (p1, p2 : URecords.point; TestZ : boolean) : boolean;
BEGIN
  result := PointsEqual (p1, p2, 0.00001, TestZ);
END;

FUNCTION PointsEqual (p1, p2 : URecords.point; delta : double) : boolean;
BEGIN
	result := PointsEqual (p1, p2, delta, false);
END;


FUNCTION PointsEqual (p1, p2 : URecords.point) : boolean;
BEGIN
	result := PointsEqual (p1, p2, 0.00001, false);
END;


FUNCTION PointsEqual (pnt1, pnt2, pnt3 : point; delta : double; TestZ : boolean) : boolean;
begin
  result := RealEqual (pnt1.x, pnt2.x, delta) and RealEqual(pnt1.y, pnt2.y, delta) and ((not testZ) or RealEqual (pnt1.z, pnt2.z, delta)) and
            RealEqual (pnt1.x, pnt3.x, delta) and RealEqual(pnt1.y, pnt3.y, delta) and ((not testZ) or RealEqual (pnt1.z, pnt2.x, delta)) and
            RealEqual (pnt2.x, pnt3.x, delta) and RealEqual(pnt2.y, pnt3.y, delta) and ((not testZ) or RealEqual (pnt2.z, pnt3.z, delta));
end;

FUNCTION PointsEqual (pnt1, pnt2, pnt3 : point; TestZ : boolean) : boolean;
BEGIN
  result := PointsEqual (pnt1, pnt2, pnt3, 0.00001, TestZ);
END;

FUNCTION PointsEqual (p1, p2 : URecords.point2D; delta : double) : boolean;
BEGIN
  result := RealEqual (p1.x, p2.x, delta) and RealEqual (p1.y, p2.y, delta);
END;
FUNCTION PointsEqual (p1, p2 : URecords.point2D) : boolean;
BEGIN
  result := PointsEqual (p1, p2, 0.00001);
END;


function AddPoints (const pnt1, pnt2 : point) : point;
begin
  addpnt (pnt1, pnt2, result);
end;

function MulPoint (const pnt1 : point; multiplier : double) : point;
begin
  result.x := pnt1.x * multiplier;
  result.y := pnt1.y * multiplier;
  result.z := pnt1.z * multiplier;
end;

function NewPoint (const xval, yval, zval : double) : point;  overload;
begin
  result.x := xval;
  result.y := yval;
  result.z := zval;
end;

function NewPoint (const xyval : point; zval : double) : point; overload;
begin
  result.x := xyval.x;
  result.y := xyval.y;
  result.z := zval;
end;

function NewPoint (const xyval : Point2D; zval : double) : point; overload;
begin
  result.x := xyval.x;
  result.y := xyval.y;
  result.z := zval;
end;

function NewPoint (const xyval : Point2D) : point; overload;
begin
  result.x := xyval.x;
  result.y := xyval.y;
  result.z := zero;
end;

function SubPnt2D (const p1, p2 : point) : Point2D;    overload;
begin
  result.x := p1.x - p2.x;
  result.y := p1.y - p2.y;
end;
function SubPnt2D (const p1, p2 : Point2D) : Point2D;    overload;
begin
  result.x := p1.x - p2.x;
  result.y := p1.y - p2.y;
end;
function SubPnt2D (const p1 : Point2D;
                          const p2 : point) : Point2D;    overload;
begin
  result.x := p1.x - p2.x;
  result.y := p1.y - p2.y;
end;

function NewPnt2D (const pnt : point) : Point2D;
begin
  result.x := pnt.x;
  result.y := pnt.y;
end;

function Distance2 (const p1, p2 : point) : double;
begin
  result := Sqr(p1.x-p2.x) + Sqr (p1.y-p2.y);
end;


function isUnityMatrix (const mat : modmat) : boolean;
var
  i, j : integer;
begin
  result := true;
  for i := 1 to 4 do
    for j := 1 to 4 do begin
      if (i=j) then begin
        if not RealEqual (mat[i][j], 1) then begin
          result := false;
          exit;
        end;
      end
      else begin
        if not RealEqual (mat[i][j], 0) then begin
          result := false;
          exit;
        end;
      end;
    end;
end;




/// <summary> calculate a transformation that can be applied to move the passed pntarr to a horizontal plane
/// </summary>
/// <param name="pnts">The pntarr to process.  If should represent planar points (if not then the returned matrix may move as few as 3 points to the horizontal plane).
/// <returns>A modelling matrix that will transform the pntarr to a horizontal plane.
/// </returns>
function GetHorizontalMat (const pnts : pntarr; npnts : integer) : modmat;
VAR
	i			: integer;
	minp, maxp	: point;
	tanang, ang	: double;
	miny, maxy	: double;
	matrotdone	: boolean;
	mod_pnts	: pntarr;
BEGIN

  matrotdone := false;
  // find the max & min x values of the points in the surface polygon - then use the corresponding points to create
  // a rotation transformation around the y axis
  {$region 'Get pnts extents into minp, maxp'}
  minp := pnts[1];
  maxp := pnts[1];
  for i := 2 to npnts do begin
    if pnts[i].x < minp.x then
      minp.x := pnts[i].x;
    if pnts[i].y < minp.y then
      minp.y := pnts[i].y;
    if pnts[i].z < minp.z then
      minp.z := pnts[i].z;
    if pnts[i].x > maxp.x then
      maxp.x := pnts[i].x;
    if pnts[i].y > maxp.y then
      maxp.y := pnts[i].y;
    if pnts[i].z > maxp.z then
      maxp.z := pnts[i].z;
  end;
  {$endregion 'Get pnts extents into minp, maxp'}

  // check that minp & maxp x & y values are different.  If not then surface is vertical and parallel
  // to either the y or x axis, so just a single rotation of -90 degrees around the y or x axis is required.
  if RealEqual (minp.x, maxp.x, 0.01) then begin
    setrotate (result, -halfpi, y);  //Matrix rotation done (parralel to x-axis)
    matrotdone := true;
  end
  else if RealEqual (minp.y, maxp.y, 0.01) then begin
    setrotate (result, -halfpi, x);  //Matrix rotation done (parralel to y-axis)
    matrotdone := true;
  end
  else if RealEqual (minp.z, maxp.z, 0.01) then begin
    setident (result);		// no rotation required
    matrotdone := true;
  end
  else begin
    // set minp & Maxp to the actual points with min & max x values.
    minp := pnts[1];
    maxp := minp;
    FOR i := 2 to npnts DO begin
      if pnts[i].x < minp.x then begin
        minp := pnts[i];
      end;
      if pnts[i].x > maxp.x then begin
        maxp := pnts[i];
      end;
    END;

    // rotate about the z axis so that the line between max & min is
    // parallel to the x axis
    tanang := (maxp.y - minp.y)/(maxp.x - minp.x);
    ang := arctan(tanang);
    setrotrel (result, -ang, z, minp);

    // set up mod_pnts by applying the transformation to surface
    mod_pnts := pnts;
    miny := 0;
    maxy := 0;
    FOR i:=1 to npnts DO BEGIN
      xformpt (pnts[i], result, mod_pnts[i]);
      if i= 1 then begin
        miny := mod_pnts[1].y;
        maxy := mod_pnts[1].y;
      end
      else begin
        miny := min (miny, mod_pnts[i].y);
        maxy := max (maxy, mod_pnts[i].y);
      end;
    END;

    if RealEqual (miny, maxy, 0.01) then begin
      // it is vertical, and now parallel to the x axis, so just needs
      // to be rotated about the x axis;
      catrotate (result, -halfpi, x);
      matrotdone := true;  //Matrix rotation done
    end
    else begin
      // not vertical, calculate rotation about the y axis
      xformpt (minp, result, minp);
      xformpt (maxp, result, maxp);
      tanang := (maxp.z - minp.z)/(maxp.x - minp.x);
      ang := arctan(tanang);
      catrotrel (result, ang, y, minp);
    end;
  end;

  // set up mod_pnts by applying the transformation to pnts
  mod_pnts := pnts;
  FOR i:=1 to npnts DO
    xformpt (pnts[i], result, mod_pnts[i]);

  // add a rotation around the x axis if required
  // find the max & min y values of the points in the mod_pnts polygon - then use the corresponding points
  // to create a rotation transformation around the y axis
  IF not matrotdone THEN begin
    minp := mod_pnts[1];
    maxp := minp;
    FOR i := 2 to npnts DO begin
      if mod_pnts[i].y < minp.y then begin
        minp := mod_pnts[i];
      end;
      if mod_pnts[i].y > maxp.y then begin
        maxp := mod_pnts[i];
      end;
    END;
    tanang := (maxp.z - minp.z	)/(maxp.y - minp.y);
    ang := arctan(tanang);
    catrotate (result, -ang, x);
  END;
END;

FUNCTION DisStr (dis : double) : string;
var
  s : shortstring;
begin
  cvdisst (dis, s);
  result := string(s);
end;


FUNCTION ColourName (clr : longint) : shortstring;
VAR
  RBGClr : Integer;
BEGIN
  if clr = 0 then
    result := ''
  else if (clr < 256) then begin
    clrGetName (clr, result);
  end	
  else begin
    RBGClr := DCADRGBtoRGB(clr);
    result := shortstring (IntToStr(GetRValue(RBGClr)) + '/' +
                           IntToStr(GetGValue(RBGClr)) + '/' +
                           IntToStr(GetBValue(RBGClr)));
  end;
END;



PROCEDURE DrawIfOn (ent : entity; drMode : integer);
BEGIN
  if lyr_ison (ent.lyr) then
    ent_draw (ent, drMode);
END;


procedure Hilite (ent : entity; clr : integer; ltype : integer = ltype_dashed);
var
  svShowWgt : boolean;
begin
  if isnil (ent.addr) then
    exit;
  svShowWgt := PGSavevar.showwgt;
  PGSavevar.showwgt := true;
  if clr = 0 then begin
    ent.width := ent.Width+1;
    ent_draw (ent, drmode_black);
    ent.Width := ent.Width-1;
    ent_draw (ent, drmode_white);
  end
  else begin
    ent.Width := ent.Width + 1;
    ent_draw (ent, drmode_black);
    ent.color := clr;
    ent.ltype := ltype;
    ent.spacing := pixsize * 26;
    ent_draw (ent, drmode_white);
  end;
  PGSavevar.showwgt := svShowWgt;
end;


{$OPTIMIZATION OFF}
{$HINTS OFF}
procedure MyModeInit (var mode : mode_type);
var
  filler : byte;  // without this filler a mode_init call can currupt
                  // memory adjacent to the mode variable.
  mymode : mode_type;
begin
  mode_init(mymode);
  mode := mymode;
end;
{$HINTS ON}
{$OPTIMIZATION ON}


procedure ValidFileName (var filename : string);
begin
  filename.Replace('''','.');
  filename.Replace('"', '.');
  filename.Replace('/', '-');
  filename.Replace('\\', '-');
  filename.Replace('>', '-');
  filename.Replace('<', '-');
  filename.Replace(':', '_');
  filename.Replace('|', '_');
  filename.Replace('*', ' ');
  filename.Replace('?', ' ');
end;


procedure ReversePnts (var pnts : TArray<point>);
var
  i : integer;
begin
  for i := 0 to (length(pnts)-2) div 2 do begin
    SwapPnt (pnts[i], pnts[length(pnts)-1-i]);
  end;
end;

end.

