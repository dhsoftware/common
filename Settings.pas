unit Settings;

interface

uses inifiles, SysUtils, Dialogs, Controls, System.IOUtils, URecords, UInterfaces,
     UConstants, System.Classes, System.UITypes, WinAPI.Windows, Vcl.Forms (*, CommonStuff*);

const
  MainInifolder = 'dhsoftware';
  MainIniName = 'dhsoftware.ini';

FUNCTION InitSettings (MacroName : string) : boolean;
PROCEDURE SaveIniInt (name : string;  value : integer; Section : string = '');   overload;
PROCEDURE SaveIniInt (name : atrname;  value : integer);  overload;
PROCEDURE SaveInt (name : atrname;  value : integer; toini : boolean);  overload;
PROCEDURE SaveInt (name : atrname;  value : integer);  overload;
PROCEDURE SaveInt (name : string;  value : integer);  overload;
FUNCTION GetIniInt (name : atrname; default : integer; Section : string = '') : integer;
FUNCTION GetSvdInt (name : atrname; default : integer) : integer; overload;
FUNCTION GetSvdInt (name : string; default : integer) : integer; overload;
PROCEDURE SaveStr (name : atrname; value : string; toini : boolean; inionly : boolean = false); overload;
PROCEDURE SaveStr (name : atrname; value : string); overload;
FUNCTION GetIniStr (name : atrname; default : string) : string;
FUNCTION GetSvdStr (name : string; default : string; useIni : boolean) : string;  overload;
FUNCTION GetSvdStr (name : string; default : string) : string;   overload;
PROCEDURE SavePnt (name : atrname;  value : URecords.point);
FUNCTION GetSvdPoint (name : atrname; var pnt : URecords.point) : boolean;
PROCEDURE writeIniStr (iniFileName : string; section : string; ident : string; value : string); overload;
PROCEDURE writeIniStr (iniFileName : string; section : string; ident : atrname; value : shortstring); overload;

PROCEDURE SaveRl (name : atrname; value : double; toIni : boolean);
FUNCTION GetSvdRl (name : atrname; default : double) : double;

PROCEDURE SaveBool (name : atrname;  value : boolean; toini : boolean = true; inionly : boolean = false);  overload;
FUNCTION GetSvdBool (name : atrname; default : boolean; fromDwg : boolean=true; fromIni : boolean = true) : boolean;

PROCEDURE SaveStrToMainIni (const  Ident, val : string);
FUNCTION GetStrFromMainIni (const Ident, Default : string) : string;

PROCEDURE SetFormPos (var form : TForm);
PROCEDURE SaveFormPos (var form : TForm);

implementation


VAR
	iniFileName	: string;
	section			: string;
	atr					: attrib;
	initialised	: boolean;
  OldMacroPath : string;
  settingFormPos : boolean;

FUNCTION NameTooLong (name : string) : boolean;
BEGIN
  result := length(name) > 12;
  if result then
    MessageDlg ('Atr Name (' + name + ') too long' + sLineBreak + 'Kindly report this error to dhSoftware.', mtError, [mbOK], 0);
END;

FUNCTION StrTooLongForAtr (value : string; name : atrname; OnlySaveOption : boolean) : boolean;
var
  atr : attrib;
BEGIN
  result := length(value) > 255;
  if OnlySaveOption then
    MessageDlg ('The following string is too long to be saved:'
                  + sLineBreak + sLineBreak + value + sLineBreak, mtError, [mbOK], 0)
  else begin
    // delete any existing atr from the drawing so that it will pick up the ini file setting;
    if atr_sysfind (name, atr) then
      atr_delsys (atr);
  end;
END;

PROCEDURE writeIniInt (iniFileName : string; section : string; ident : string; value : integer);  overload;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFileName);
  try
    iniFile.WriteInteger(section, ident, value);
  finally
    iniFile.Free;
  end;
END;

PROCEDURE writeIniInt (iniFileName : string; section : string; ident : atrname; value : integer); overload;
BEGIN
  writeIniInt(iniFileName, section, string(ident), value);
END;

PROCEDURE writeIniStr (iniFileName : string; section : string; ident : string; value : string);
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    iniFile.WriteString(section, ident, value);
  finally
    iniFile.Free;
  end;
END;

PROCEDURE writeIniStr (iniFileName : string; section : string; ident : atrname; value : shortstring);
BEGIN
  writeIniStr (iniFileName, section, string(ident), string(value));
END;

PROCEDURE writeIniReal (iniFileName : string; section : string; ident : atrname; value : Double);
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    iniFile.WriteFloat(section, string(ident), value);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : shortstring) : shortstring;     overload;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := shortstring(iniFile.ReadString(section, string(ident), string(default)));
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : string) : string;     overload;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := iniFile.ReadString(section, string(ident), default);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniInt (iniFileName : string; section : string; ident : atrname;
                    default : integer) : integer;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := iniFile.ReadInteger(section, string(ident), default);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniReal (iniFileName : string; section : string; ident : atrname;
                    default : Double) : Double;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := iniFile.ReadFloat(section, string(ident), default);
  finally
    iniFile.Free;
  end;
END;

FUNCTION InitSettings (MacroName : string) : boolean;
VAR
	MacroPath, SupPath : shortstring;
  MacroPath1, prompt, OldIniFile, TempStr : string;


BEGIN
  result := false;
	section := string(macroName);
	UInterfaces.getpath (SupPath, pathsup);
  if not SysUtils.DirectoryExists (string(SupPath) + MainInifolder) then begin
    if not SysUtils.CreateDir(string(SupPath) + MainInifolder) then begin
      prompt := 'Failed to create required directory (' + string(SupPath) + MainInifolder + ')'
                + sLineBreak + sLineBreak + 'Macro will exit';
      messagedlg (prompt, mtError, [mbOK], 0);
      exit;
    end;
  end;

	iniFileName := string(SupPath) + MainInifolder + '/' + MainIniName;

  try
    UInterfaces.getpath (MacroPath, pathmcr);
    if not  fileexists (inifilename) then
      writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));

    OldMacroPath := readIniStr (iniFileName, Section, 'PathMCR', '');
    MacroPath1 := OldMacroPath;
    writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));

    if length (MacroPath1) = 0 then begin
      writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));
      MacroPath1 := string(MacroPath);
    end;

    iniFileName := string(MacroPath) + macroName + '.ini';

    if CompareText (string(MacroPath), MacroPath1) <> 0 then begin
      OldIniFile := MacroPath1 + macroName + '.ini';
      if FileExists (OldIniFile) and FileExists (iniFileName) then begin
        prompt := 'Settings for this macro are stored in a file in your macro path.' + sLineBreak + sLineBreak;
        prompt := prompt + 'Last time you used the macro this path was:' + sLineBreak + MacroPath1 + sLineBreak + sLineBreak;
        prompt := prompt + 'It is now:' + sLineBreak + string(MacroPath) + sLineBreak + sLineBreak;
        prompt := prompt + 'Would you like to copy settings from the previous path to the current one?';

        if messagedlg (prompt, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
          TempStr := iniFileName + '-old';
          if FileExists (TempStr) then
            SysUtils.DeleteFile (TempStr);
          RenameFile (iniFileName, TempStr);
          TFile.Copy (string(OldIniFile), iniFileName);
        end
        else if FileExists (OldIniFile) and not FileExists (iniFileName) then // just copy old file if new file does not exist
          TFile.Copy (string(OldIniFile), iniFileName);
      end;
    end;
    initialised := true;
    settingFormPos := false;
    result := true;
  except
    prompt := 'Initialisation Failure (' + prompt + ')'
                + sLineBreak + sLineBreak + 'Macro will exit';
    messagedlg (prompt, mtError, [mbOK], 0);
  end;
END;


FUNCTION PrevMacroPath : String;
BEGIN
  result := OldMacroPath;
END;

PROCEDURE SaveIniInt (name : string;  value : integer; Section : string = '');
BEGIN
  if Section > '' then
    writeIniInt (iniFileName, Section, name, value)
  else
		writeIniInt (iniFileName, section, name, value);
END;

PROCEDURE SaveIniInt (name : atrname;  value : integer);
BEGIN
		writeIniInt (iniFileName, section, name, value);
END;

PROCEDURE SaveInt (name : atrname;  value : integer; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_int;
		atr.int := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_int);
		atr.name := name;
		atr.int := value;
		atr_add2sys (atr);
	end;
	if toini then
		SaveIniInt(name, value);
END;

PROCEDURE SaveInt (name : atrname;  value : integer);
BEGIN
  SaveInt (name, value, true);
END;

PROCEDURE SaveInt (name : string;  value : integer);
BEGIN
  if not NameTooLong(name) then
  SaveInt (atrname(name), value, true);
END;

FUNCTION GetIniInt (name : atrname; default : integer; Section : string = '') : integer;
BEGIN
  if Section > '' then
    result := readIniInt (iniFileName, Section, name, default)
  else
		result := readIniInt (iniFileName, section, name, default);
END;

FUNCTION GetSvdInt (name : atrname; default : integer) : integer;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.int
	else
		result := GetIniInt (name, default);
END;

FUNCTION GetSvdInt (name : string; default : integer) : integer;
BEGIN
	if NameTooLong(name) then
    result := 0
  else
		result := GetSvdInt (atrname(name), default);
END;


PROCEDURE SetFormPos (var form : TForm);
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
  settingFormPos := true;
  try
    UInterfaces.getpath (SupPath, pathsup);
    iniFileName := string(SupPath) + MainIniFolder + '\\' + MainIniName;
    if fileexists (inifilename) then begin
      iniFile := TIniFIle.Create(iniFileName);
      try
        try     { not sure why setting form size is sometimes causing access violation. Have added try/except
                  logic below to counter this }
          form.left := iniFile.ReadInteger(section, form.name + '-left', form.left);
        except
        end;
        try
          form.width := iniFile.ReadInteger(section, form.name + '-width', form.width);
        except
        end;
        try
          form.top := iniFile.ReadInteger(section, form.name + '-top', form.top);
        except
        end;
        try
          form.height := iniFile.ReadInteger(section, form.name + '-height', form.Height);
        except
        end;
      finally
        iniFile.Free;
      end;
    end;
  except

  end;
  settingFormPos := false;
END;

PROCEDURE SaveFormPos (var form : TForm);
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
  if not settingFormPos then begin
    UInterfaces.getpath (SupPath, pathsup);
    iniFileName := string(SupPath) + MainIniFolder + '/' + MainIniName;
    if fileexists (inifilename) then begin
      iniFile := TIniFIle.Create(iniFileName);
      try
        iniFile.WriteInteger(section, form.name + '-left', form.left);
        iniFile.WriteInteger(section, form.name + '-width', form.width);
        iniFile.WriteInteger(section, form.name + '-top', form.top);
        iniFile.WriteInteger(section, form.name + '-height', form.height);
      finally
        iniFile.Free;
      end;
    end;
  end;
END;

PROCEDURE SaveStr (name : atrname; value : string; toini : boolean; inionly : boolean = false); overload;
VAR
  s, s1 : str255;
BEGIN
  if length(value) <= 255 then begin
    s := str255 (value);
    s1 := '';
  end
  else begin
    s := str255 (value.Substring(0, 255));
    s1 := str255 (value.Substring(255, 255));
  end;
  if not inionly then begin
    if atr_sysfind (name, atr) then begin
      atr.atrtype := atr_str255;
      atr.shstr := s;
      atr_update (atr);
    end
    else begin
      atr_init (atr, atr_str255);
      atr.name := name;
      atr.shstr := s;
      atr_add2sys (atr);
    end;
    if length(s1) > 0 then begin
      name := name + '^';
      if atr_sysfind (name, atr) then begin
        atr.atrtype := atr_str255;
        atr.shstr := s1;
        atr_update (atr);
      end
      else begin
        atr_init (atr, atr_str255);
        atr.name := name;
        atr.shstr := s1;
        atr_add2sys (atr);
      end;
    end;
  end;
  if toini or inionly then
	  writeIniStr(iniFileName, section, name, s);
END;

PROCEDURE SaveStr (name : atrname; value : string);
BEGIN
  SaveStr (name, value, true);
END;

FUNCTION GetIniStr (name : atrname; default : string) : string;
BEGIN
  result := readIniStr (iniFileName, section, name, default);
END;

FUNCTION GetSvdStr (name : string; default : string; useIni : boolean) : string;
BEGIN
  if NameTooLong(name) then
    result := default
  else begin
    if atr_sysFind (atrname(name), atr) and (atr.atrtype = atr_str255) then begin
      result := string(atr.shstr);
      if length (result) = 255 then begin   // look for a continuation atr with same name + '^'
        name := name + '^';
        if atr_sysFind (atrname(name), atr) and (atr.atrtype = atr_str255) then begin
          result := result + string(atr.shstr);
        end;
      end;
    end
    else begin
      if useIni then
		    result := GetIniStr (atrname(name), default)
      else
        result := default;
    end;
  end;
END;

FUNCTION GetSvdStr (name : string; default : string) : string;
BEGIN
  result := GetSvdStr (name, default, true);
END;


PROCEDURE SaveRl (name : atrname; value : double; toIni : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_rl;
		atr.rl := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_rl);
		atr.name := name;
		atr.rl := value;
		atr_add2sys (atr);
	end;
	if toIni then
    writeIniReal(iniFileName, section, name, value);
END;

FUNCTION GetSvdRl (name : atrname; default : double) : double;
BEGIN
	if atr_sysfind (name, atr) and (atr.atrtype = atr_rl) then
		result := atr.rl
	else
		result := readIniReal(iniFileName, section, name, default);
END;


PROCEDURE SavePnt (name : atrname;  value : URecords.Point);
BEGIN
	if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_pnt;
		atr.pnt := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_pnt);
		atr.name := name;
		atr.pnt := value;
		atr_add2sys (atr);
	end;
END;

FUNCTION GetSvdPoint (name : atrname; var pnt : URecords.point) : boolean;
BEGIN
	if atr_sysfind (name, atr) and (atr.atrtype = atr_pnt) then begin
    pnt := atr.pnt;
    result := true
  end
  else
    result := false;
END;


PROCEDURE SaveBool (name : atrname;  value : boolean; toini : boolean = true; inionly : boolean = false);  overload;
BEGIN
  if not inionly then begin
    if atr_sysfind (name, atr) then begin
      atr.atrtype := atr_int;
      atr.int := ord(value);
      atr_update (atr);
    end
    else begin
      atr_init (atr, atr_int);
      atr.name := name;
      atr.int := ord(value);
      atr_add2sys (atr);
    end;
  end;

  if toini or inionly then
    SaveIniInt(name, ord(value));
END;

FUNCTION GetSvdBool (name : atrname; default : boolean; fromDwg : boolean=true; fromIni : boolean = true) : boolean;
BEGIN
  if fromDwg and atr_sysfind (name, atr) and (atr.atrtype = atr_int) then
    result := boolean(atr.int)
  else if fromIni then
    result := boolean (readIniInt(iniFileName, section, name, ord(default)))
  else
    result := default;
END;

PROCEDURE SaveStrToMainIni (const  Ident, val : string);
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
  UInterfaces.getpath (SupPath, pathsup);
  iniFileName := string(SupPath) + MainIniFolder + '/' + MainIniName;
  if fileexists (inifilename) then begin
    iniFile := TIniFIle.Create(iniFileName);
    try
      iniFile.WriteString(section, ident, val);
    finally
      iniFile.Free;
    end;
  end;
END;

FUNCTION GetStrFromMainIni (const Ident, Default : string) : string;
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
  try
    UInterfaces.getpath (SupPath, pathsup);
    iniFileName := string(SupPath) + MainIniFolder + '\\' + MainIniName;
    if fileexists (inifilename) then begin
      iniFile := TIniFIle.Create(iniFileName);
      try
        try
          result := iniFile.ReadString (section, ident, default);
        except
          result := default;
        end;
      finally
        iniFile.Free;
      end;
    end
    else
      result := default;
  except
    result := default;
  end;
END;


END.



